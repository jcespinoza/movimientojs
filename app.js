var app = angular.module('app', []);

app.controller('MovimientoHorizontalController', function($scope){
	//Variables para Movimiento Horizontal
	$scope.limpiar = function(){
		$scope.tiempo = null;
		$scope.posicionInicial = null;
		$scope.velocidadInicial = null;
		$scope.posicionFinal = null;
	}

	$scope.limpiar();
});

app.controller('TiempoHorizontalController', function($scope){
	//Variables para Tiempo Horizontal
	$scope.limpiar = function(){
		$scope.tiempo = null;
		$scope.posicionInicial = null;
		$scope.velocidadInicial = null;
		$scope.posicionFinal = null;
	}

	$scope.limpiar();
});

app.controller('MovimientoVerticalController', function($scope){
	//Variables para Movimiento Vertical
	$scope.limpiar = function(){
		$scope.tiempo = null;
		$scope.posicionInicial = null;
		$scope.velocidadInicial = null;
		$scope.gravedad = 9.8;
		$scope.posicionFinal = null;
	}

	$scope.limpiar();
});

app.controller('TiempoVerticalController', function($scope){
	$scope.Math = window.Math;

	//Variables para Tiempo Vertical
	$scope.limpiar = function(){
		$scope.tiempo1 = null;
		$scope.tiempo2 = null;
		$scope.posicionInicial = null;
		$scope.velocidadInicial = null;
		$scope.gravedad = 9.8;
		$scope.posicionFinal = null;
	}

	$scope.limpiar();
});